package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	urlPrefix := os.Getenv("STARCOMM_JL")
	url := urlPrefix + "/discovery"
	totalCalls := getTotalCalls()
	filePrefix := fmt.Sprintf("%s_%d", makeFilePrefix(url), totalCalls)

	queue := make(chan int, totalCalls)
	result := make(chan string, totalCalls)

	// Define workers
	for w := 1; w <= totalCalls; w++ {
		go makeRequest(url, queue, result, w, totalCalls)
	}

	for q := 1; q <= totalCalls; q++ {
		queue <- q
	}
	close(queue)

	for r := 1; r <= totalCalls; r++ {
		writeToFile(<-result, fmt.Sprint(filePrefix))
	}
}

func makeRequest(url string, jobs <-chan int, result chan<- string, id int, calls int) {
	for j := range jobs {
		fmt.Println("worker", id, "started  job", j)
		var jsonStr = []byte(`{"from": "Saru", "message": "Saru to Discovery. Over!"}`)
		req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		client := &http.Client{}
		start := time.Now()
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		secs := time.Since(start).Seconds()
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
			result <- fmt.Sprintf("%d, %d, %.2f, %d, %s\n", id, calls, secs, len(body), url)
		}

	}
}

func getTotalCalls() int {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter total calls: ")
	input, _ := reader.ReadString('\n')
	input = strings.TrimSpace(input)
	callInt, _ := strconv.Atoi(input)
	return callInt
}

func writeToFile(s string, namePrefix string) {
	fileStore := fmt.Sprintf("./data/%s.log", namePrefix)
	f, err := os.OpenFile(fileStore, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.WriteString(s); err != nil {
		log.Println(err)
	}
}

func makeFilePrefix(s string) string {
	strSlice := strings.Split(s, "-")
	str := strSlice[0] + strSlice[1]
	str = strings.ReplaceAll(str, "https://", "")
	return str

}
